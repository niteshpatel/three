/**
 * Created with JetBrains WebStorm.
 * User: Nitesh
 * Date: 16/03/13
 * Time: 12:37
 * To change this template use File | Settings | File Templates.
 */

var three = {

    // Constants - Perspective
    ORIGIN_OFFSET: 300,

    // Constants - Car
    CAR_WIDTH: 60,
    CAR_DEPTH: 80,
    CAR_HEIGHT: 0,
    CAR_SPEED: 10,

    // Constants - Obstacles
    OB_WIDTH: 100,
    OB_DEPTH: 40,
    OB_HEIGHT: 0,
    OB_RATE: 40,

    // Constants - Road lines
    ROAD_LINE_WIDTH: 16,
    ROAD_LINE_DEPTH: 80,
    ROAD_LINE_HEIGHT: 0,

    // Constants - Relative speeds
    BG_SPEED: 6,
    OB_SPEED: 9,
    SPEEDUP_RATE: 20,

    // Constants - Application speed (game loop timing)
    INTERVAL: 8,

    // Constants - Initial values
    INITIAL_GAME_SPEED: 10,
    INITIAL_TIMER: 30,

    // Game mode/screen
    game_mode: 0,

    // Car position
    carPosX: -40,
    carPosY: 600,

    // Background position
    bgPosY: 0,

    // Game speed
    game_speed: 0,

    // Game loop counter used to manage timings
    count: 0,

    // Counter since last stop
    countSinceStop: 0,

    // Array to manage obstacles
    obstacles: [],

    // Array to managed current inputs
    pressed: [],

    // Boolean which is true when car is not stopped
    moving: true,

    // Score and timer
    score: 0,
    timer: 0,

    // Refresh the display
    repaint: function () {

        // Clear canvas
        this.c2d.clearRect(0, 0, this.canvas.width, this.canvas.height);

        // Draw background
        this.drawBackground();

        // Draw obstacles
        this.drawForeground();

        // Draw car
        this.drawCuboid(this.carPosX, this.carPosY, this.CAR_WIDTH, this.CAR_DEPTH, this.CAR_HEIGHT);

        // Update score
        $("#timer").text(this.timer.toFixed(2));
        $("#score").text(this.score);
    },

    // Check collision for the given objects
    checkCollision: function (a, b) {
        return !(a.posX > b.posX + b.width ||
            b.posX > a.posX + a.width ||
            b.posY < a.posY - (a.depth * this.getRatio(a.posY)) ||
            a.posY < b.posY - (b.depth * this.getRatio(b.posY)));
    },

    // Check collisions between all objects
    checkBlocked: function (posX, posY) {

        // Check if we have any collisions
        for (var i = 0; i < this.obstacles.length; i++)
            if (this.checkCollision(
                {
                    'posX': this.obstacles[i][0],
                    'posY': this.obstacles[i][1],
                    'width': this.OB_WIDTH,
                    'depth': this.OB_DEPTH
                },
                {
                    'posX': posX,
                    'posY': posY,
                    'width': this.CAR_WIDTH,
                    'depth': this.CAR_DEPTH
                }
            ))
                return true;

        // If no collisions return false
        return false;
    },

    // Draw background/obstacles
    drawForeground: function () {

        // Check if we should move the background/obstacles
        this.moving = !this.checkBlocked(this.carPosX, this.carPosY - this.OB_SPEED);

        // If we are not blocked, calculate the new positions
        if (this.moving) {
            // Create a new obstacle every OB_RATE steps
            if (this.count % (this.OB_RATE * this.game_speed) == 0) {
                this.obstacles.push([
                    // Space them randomly both vertically and horizontally
                    ((Math.random() - 0.5) * (this.canvas.width - this.OB_WIDTH)) - (this.OB_WIDTH / 2),
                    -Math.random() * this.OB_DEPTH]);
            }

            // Move the background and obstacles based on game speed
            if (this.count % this.game_speed == 0) {

                // Move the background
                this.bgPosY = (this.bgPosY + this.BG_SPEED) % (this.canvas.height * 2);

                // Move the obstacles
                for (var i = 0; i < this.obstacles.length; i++) {
                    this.obstacles[i][1] += this.OB_SPEED * this.getRatio(this.obstacles[i][1]);
                }
            }
        }

        // Draw the road lines
        for (var j = (this.canvas.height * 2) + this.bgPosY; j > 0; j -= this.canvas.height / 4)
            this.drawCuboid(
                -this.ROAD_LINE_WIDTH / 2,
                j * this.getRatio(j),
                this.ROAD_LINE_WIDTH,
                this.ROAD_LINE_DEPTH,
                this.ROAD_LINE_HEIGHT);

        // Draw the obstacles
        for (var k = 0; k < this.obstacles.length; k++)
            this.drawCuboid(
                this.obstacles[k][0],
                this.obstacles[k][1],
                this.OB_WIDTH,
                this.OB_DEPTH,
                this.OB_HEIGHT);

        // Remove obstacles that are off the bottom of the screen
        if (this.obstacles[-1] && (this.obstacles[-1][1] > this.canvas.height))
            this.obstacles.pop();
    },

    // Draw static background
    drawBackground: function () {

        // Draw bottom edge
        this.c2d.beginPath();
        this.c2d.moveTo(0, this.canvas.height);
        this.c2d.lineTo(this.canvas.width, this.canvas.height);
        this.c2d.stroke();
        this.c2d.closePath();

        // Draw left edge
        this.c2d.beginPath();
        this.c2d.moveTo(this.canvas.width / 2, -this.ORIGIN_OFFSET);
        this.c2d.lineTo(0, this.canvas.height);
        this.c2d.stroke();
        this.c2d.closePath();

        // Draw right edge
        this.c2d.beginPath();
        this.c2d.moveTo(this.canvas.width / 2, -this.ORIGIN_OFFSET);
        this.c2d.lineTo(this.canvas.width, this.canvas.height);
        this.c2d.stroke();
        this.c2d.closePath()
    },

    // Calculate the perspective ratio for height/width based on vertical distance from the base
    getRatio: function (posY) {
        return (posY + this.ORIGIN_OFFSET) / (this.canvas.height + this.ORIGIN_OFFSET);
    },

    // Draw an object with weight, depth, and height (optional)
    drawCuboid: function (posX, posY, width, depth, height) {

        // Calculate lower X co-ordinates
        var ratio1 = this.getRatio(posY);
        var x00 = posX * ratio1;
        var x10 = (posX + width) * ratio1;

        // Calculate upper X co-ordinates
        var y1 = posY - (depth * ratio1);
        var ratio2 = this.getRatio(y1);
        var x01 = posX * ratio2;
        var x11 = (posX + width) * ratio2;

        // Calculate difference between rectangles
        height = height * ratio1;

        // Draw new lines
        this.c2d.beginPath();
        this.c2d.moveTo(x00 + this.canvas.width / 2, posY);
        this.c2d.lineTo(x10 + this.canvas.width / 2, posY);
        this.c2d.lineTo(x11 + this.canvas.width / 2, y1);
        this.c2d.lineTo(x01 + this.canvas.width / 2, y1);
        this.c2d.lineTo(x00 + this.canvas.width / 2, posY);
        this.c2d.stroke();
        this.c2d.closePath();

        // Draw cuboid if height > 0
        if (height == 0)
            return;

        // Draw second rectangle
        this.c2d.beginPath();
        this.c2d.moveTo(x00 + this.canvas.width / 2, posY - height);
        this.c2d.lineTo(x10 + this.canvas.width / 2, posY - height);
        this.c2d.lineTo(x11 + this.canvas.width / 2, y1 - height);
        this.c2d.lineTo(x01 + this.canvas.width / 2, y1 - height);
        this.c2d.lineTo(x00 + this.canvas.width / 2, posY - height);
        this.c2d.stroke();
        this.c2d.closePath();

        // Connect them up
        this.c2d.beginPath();
        this.c2d.moveTo(x00 + this.canvas.width / 2, posY);
        this.c2d.lineTo(x00 + this.canvas.width / 2, posY - height);
        this.c2d.moveTo(x10 + this.canvas.width / 2, posY);
        this.c2d.lineTo(x10 + this.canvas.width / 2, posY - height);
        this.c2d.moveTo(x11 + this.canvas.width / 2, y1);
        this.c2d.lineTo(x11 + this.canvas.width / 2, y1 - height);
        this.c2d.moveTo(x01 + this.canvas.width / 2, y1);
        this.c2d.lineTo(x01 + this.canvas.width / 2, y1 - height);
        this.c2d.stroke();
        this.c2d.closePath()
    },

    // Move the car
    moveCar: function () {

        // Default speed
        var speedX = 0;
        var speedY = 0;

        // Calculate Y speed ratio based on position from base
        var ratio = this.getRatio(this.carPosY);

        // UP
        if (this.pressed[38])
            speedY = -this.CAR_SPEED * ratio;
        // DOWN
        if (this.pressed[40])
            speedY = this.CAR_SPEED * ratio;
        // LEFT
        if (this.pressed[37])
            speedX = -this.CAR_SPEED;
        // RIGHT
        if (this.pressed[39])
            speedX = this.CAR_SPEED;

        // If the car is not blocked, move the car
        if (!this.checkBlocked(this.carPosX + speedX, this.carPosY + speedY)) {
            this.carPosX += speedX;
            this.carPosY += speedY;
        }

        // Limit the X position based on the canvas limits
        if (this.carPosX <= -(this.canvas.width / 2))
            this.carPosX = -(this.canvas.width / 2);
        else if (this.carPosX + this.CAR_WIDTH > (this.canvas.width / 2))
            this.carPosX = (this.canvas.width / 2) - this.CAR_WIDTH;

        // Limit the Y position based on the canvas limits
        if (this.carPosY > this.canvas.height)
            this.carPosY = this.canvas.height;
        else if (this.carPosY < 0 + ((this.CAR_DEPTH + this.CAR_HEIGHT) * ratio))
            this.carPosY = ((this.CAR_DEPTH + this.CAR_HEIGHT) * ratio);
    },

    // The game loop
    gameLoop: function () {

        // Frame count
        this.count += 1;
        if (this.count == 6000)
            this.count = 0;

        // Timer and score updates
        this.timer -= 0.01;
        if (this.moving) {
            this.score++;
            this.countSinceStop++;
        }

        // When the car is stopped reset the game speed and count since stop
        else {
            this.countSinceStop = 0;
            this.game_speed = this.INITIAL_GAME_SPEED;
        }

        // End game if timer is 0
        if (this.timer < 0) {
            window.clearInterval(this._inputInterval);
            three.game_mode = 2;
            $("#game").hide();
            $('#final_score').text($("#score").text());
            $("#stop").show();
        }

        // Update the speed based on the count since stop
        // (also limit the minimum and maximum speed)
        this.game_speed = this.INITIAL_GAME_SPEED - Math.floor(this.countSinceStop / this.SPEEDUP_RATE);
        this.game_speed = Math.max(this.game_speed, 1);

        // Move car and repaint everything
        this.moveCar();
        this.repaint();
    },

    // Setup canvas
    setupCanvas: function () {
        this.canvas = document.getElementById("canvas1");
        this.canvas.width = 640;
        this.canvas.height = window.innerHeight - 150;
        this.c2d = this.canvas.getContext("2d");
        this.c2d.lineWidth = 1;
        this.c2d.lineCap = "round";
    },

    // Reset the game and start the loop
    reset: function () {
        this.timer = this.INITIAL_TIMER;
        this.game_speed = this.INITIAL_GAME_SPEED;
        this.score = 0;
        this.countSinceStop = 0;
        this.moving = true;
        this.obstacles = [];
        this.pressed = [];
    },

    // Set up the canvas and input handling
    setup: function () {

        // Setup the canvas
        this.setupCanvas();
        $(window).resize(function () {
            three.setupCanvas();
        });

        // Setup user inputs for various game screens
        var context = $('body');
        context.keydown(function (e) {

            // Start screen
            if (three.game_mode == 0) {

                // Space starts the game
                if (e.keyCode == 32) {
                    three.game_mode = 1;
                    $("#game").show();
                    $("#start").hide();

                    // Start/restart the game
                    three.reset();
                    three._inputInterval = window.setInterval(function () {
                        three.gameLoop();
                    }, three.INTERVAL)
                }
            }

            // Game over screen
            else if (three.game_mode == 2) {

                // Space restarts
                if (e.keyCode == 32) {
                    three.game_mode = 1;
                    $("#game").show();
                    $("#stop").hide();

                    // Start/restart the game
                    three.reset();
                    three._inputInterval = window.setInterval(function () {
                        three.gameLoop();
                    }, three.INTERVAL)
                }
            }

            // Play screen
            else if (three.game_mode == 1) {

                // Use an array to manage multiple key inputs
                if (e.keyCode == 38 || e.keyCode == 40 || e.keyCode == 37 || e.keyCode == 39) {
                    three.pressed[e.keyCode] = 1;
                }
            }
        });

        // Handle key up events
        context.keyup(function (e) {

            // Play screen
            if (three.game_mode == 1) {

                // Remove the released key from the array
                if (e.keyCode == 38 || e.keyCode == 40 || e.keyCode == 37 || e.keyCode == 39)
                    three.pressed[e.keyCode] = 0;
            }
        });
    }
};
